import React from "react";
import Solutions from "@/components/solutions";
import Clients from "@/components/ui/Clients";
import Services from "@/components/services";
import IndustryFocus from "@/components/industryFocus";
import Footer from "@/components/footer";
import InfotechWeekly from "@/components/infotechWeekly";
import NewsLetter from "@/components/newsLetter";
import ClientTestimonial from "@/components/clientTestimonial";

export default function Home() {
  return (
    <main >
      <Solutions/>
      <Clients/>
      <Services/>
      <IndustryFocus/>
      <ClientTestimonial/>
      <InfotechWeekly/>
      <NewsLetter/>
      <Footer/>
    </main>
  );
}
