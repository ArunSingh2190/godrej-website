import Link from 'next/link'
import React from 'react'

const Page = () => {
  return (
    <div className='container mx-auto'>
        <h1 className="pt-20 pb-20 text-lg">
            Cloudification Page
            <br/>
            <Link href="/" className='text-blue-500'> Home</Link>
        </h1>
        
    </div>
  )
}

export default Page