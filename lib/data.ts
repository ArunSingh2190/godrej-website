import logo1Img from "@/public/logo/logo1.png";
import logo2Img from "@/public/logo/logo2.png";
import logo3Img from "@/public/logo/logo3.png";
import logo4Img from "@/public/logo/logo4.png";

export const menuLinks = [
    {
        name: "Solutions",
        hash: "#solutions",
    },
    {
        name: "Services",
        hash: "#services",
    },
    {
        name: "Industry Focus",
        hash: "#industry-focus",
    },
    {
        name: "About Us",
        hash: "#about-us",
    },
    {
        name: "Projects",
        hash: "#projects",
    },
    {
        name: "Careers",
        hash: "#careers",
    },
    {
        name: "Insights",
        hash: "#insights",
    },
] as const;

export const companyLogos = [
    {
        id: 1,
        imageUrl: logo1Img,
    },
    {
        id: 1,
        imageUrl: logo2Img,
    },
    {
        id: 1,
        imageUrl: logo3Img,
    },
    {
        id: 1,
        imageUrl: logo4Img,
    },
] as const;