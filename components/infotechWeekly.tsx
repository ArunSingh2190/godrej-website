import Image from 'next/image'
import Link from 'next/link'
import React from 'react'

const InfotechWeekly = () => {
  return (
    <div className='bg-[#F2F4F7]'>
        <div className='pl-20 ml-20 pb-16'>
            <h5 className='pt-16 font-bold underline underline-offset-4 text-[#0745D3]'> WEEKLY UPDATES </h5>
            <div className='mt-4 grid grid-cols-2 gap-4'>
                <h2 className='text-3xl font-bold'>
                    <span>Infotech</span>
                </h2>
                <p className='text-sans text-[#475467] text-sm'>
                    <span>Enabling organizations to optimize cost & enhance</span> 
                    <br/>
                    <span>business processes with next-gen digital technologies</span>
                </p>
            </div>
            <div className='flex flex-col mt-8'>
                <div className='flex flex-row mb-8'>
                    <Image src="/weeklyUpdate/weeklyUpdate1.png" alt="Automation" width={400} height={400}/>
                    <div className='flex flex-col'>
                        <span className='font-light px-4'>20 jan 2023</span>
                        <span className='font-semibold mt-1 py-2 px-4'>Robust Automation for Enhanced Business Efficiency</span>
                        <p className='font-medium px-4'>
                        <span>Enabling organizations to optimize cost & enhance business</span>
                        <br/>
                        <span>processes with next-gen digital technologies like Robotic</span>
                        <br/> 
                        <span>Process Automation RPA, eCommerce & Analytics.</span>
                        </p>
                        <Link href="/automation" className='text-[#0745D3] mt-8 px-4 hover:font-bold'>Read article</Link>
                    </div>
                </div>
                <div className='flex flex-row'>
                    <Image src="/weeklyUpdate/weeklyUpdate2.png" alt="Cloudification" width={400} height={400}/>
                    <div className='flex flex-col'>
                    <span className='font-light px-4'>20 jan 2023</span>
                        <span className='font-semibold mt-1 py-2 px-4'>Robust Automation for Enhanced Business Efficiency</span>
                        <p className='font-medium px-4'>
                        <span>Enabling organizations to optimize cost & enhance business</span>
                        <br/>
                        <span>processes with next-gen digital technologies like Robotic</span>
                        <br/> 
                        <span>Process Automation RPA, eCommerce & Analytics.</span>
                        </p>
                        <Link href="/automation" className='text-[#0745D3] mt-8 px-4 hover:font-bold'>Read article</Link>
                    </div>
                </div> 
                <span className=' flex justify-center underline underline-offset-4 text-[#0745D3] opacity-100 font-bold hover:font-light' >
                    read more articles
                </span>  
            </div>
        </div>   
    </div>
  )
}

export default InfotechWeekly