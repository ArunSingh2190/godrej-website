import Image from 'next/image'
import React from 'react'
import CaseStudies from './ui/CaseStudies'

const IndustryFocus = () => {
  return (
    <section id="industry-focus">
        <div className='bg-gradient-to-r from-[#CE2EE3] via-[#7433EA] to-[#070021] bg-opacity-90 pb-20'>
            <div className='flex flex-col justify-center items-center'>
                <h2 className='text-3xl font-bold font-sans text-white mt-20'>The Hub of Expertise</h2>
                <p className='text-white font-mono pt-2 text-center'>
                    <span>
                        We are here to build edge and bring technology brillance with the finest in the industry.
                    </span>
                    <br/>
                    <span>
                        Driving theninnovative path, we develop better results for businesses across the globe.
                    </span>
                </p>
            </div>
            <div>
                <div className='flex flex-row justify-around items-center bg-gradient-to-r from-[#CE2EE3] via-[#7433EA] to-[#070021] bg-opacity-90 '>
                    <div className='flex flex-col justify-around items-center'>
                        <h2 className='text-5xl font-bold font-sans text-white mt-20'>50 +</h2>
                        <p className='text-white font-sherif pt-2 text-center'>Countries with trusted client</p>
                    </div>
                    <div className='flex flex-col justify-around items-center'>
                        <h2 className='text-5xl font-bold font-sans text-white mt-20'>10.31 +</h2>
                        <p className='text-white font-sherif pt-2 text-center'>Billion total revenue</p>
                    </div>
                    <div className='flex flex-col justify-around items-center'>
                        <h2 className='text-5xl font-bold font-sans text-white mt-20'>500 +</h2>
                        <p className='text-white font-sherif pt-2 text-center'>Employees all over the globe</p>
                    </div>
                </div>
            </div>
        </div>
        <CaseStudies/>
    </section>
  )
}

export default IndustryFocus