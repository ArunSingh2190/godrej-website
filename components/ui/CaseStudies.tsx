import Image from 'next/image'
import Link from 'next/link'
import React from 'react'

const CaseStudies = () => {
  return (
    <div>
        <div className='mt-16 pl-20 ml-20'>
            <h5 className='font-bold underline underline-offset-4 text-[#0745D3]'> CASE STUDY </h5>
            <div className='mt-4 grid grid-cols-2 gap-4'>
                <h2 className='text-3xl font-bold'>
                    <span>Delivering Results</span>
                    <br/>
                    <span> that Matter Results</span>
                </h2>
                <p className='text-sans text-[#475467] text-sm'>
                    <span>Worlds top-notch businesses choose us for our excellent</span> 
                    <br/>
                    <span>technical acumen and proven standards as we deliver high-</span>
                    <br/>
                    <span>performing multidisciplinary solutions across the spectrum</span> 
                    <br/>
                    <span>of industries.</span>
                </p>
            </div>
            <div className='flex flex-row mt-8 space-x-4'>
                <div className='flex flex-col'>
                    <Image src="/caseStudyImg/automation.png" alt="Automation" width={400} height={400}/>
                    <span className='font-semibold px-4'>Automation</span>
                    <span className='font-light mt-2 px-4'>20 jan 2023</span>
                    <p className='font-medium px-4'>
                    <span>Adhesive Manufacturer cuts open</span>
                    <br/>
                    <span>ticket volume by 25% with Managed</span>
                    <br/> 
                    <span>Services in .NET, SharePoint and PHP</span>
                    </p>
                    <Link href="/automation" className='text-[#0745D3] mt-8 px-4 hover:font-bold'>Read article</Link>
                </div>
                <div className='flex flex-col'>
                    <Image src="/caseStudyImg/cloudification.png" alt="Cloudification" width={410} height={400}/>
                    <span className='font-semibold px-4'>Cloudification</span>
                    <span className='font-light mt-2 px-4'>20 jan 2023</span>
                    <p className='font-medium px-4'>
                    <span>KSA based Coffee Brand Distributor</span>
                    <br/>
                    <span>minimizes intercompany transaction</span>
                    <br/> 
                    <span>time with Cloud-based D365 Finance, </span>
                    <br/>
                    <span>SCM & Commerce implementation</span>
                    </p>
                    <Link href="/cloudification" className='text-[#0745D3] mt-8 px-4 hover:font-bold'>Read article</Link>
                </div>
                <div className='flex flex-col'>
                    <Image src="/caseStudyImg/transformation.png" alt="Transformation" width={310} height={400}/>
                    <span className='font-semibold px-4'>Transformation</span>
                    <span className='font-light mt-2 px-4'>20 jan 2023</span>
                    <p className='font-medium px-4'>
                    <span>How Electrical Component</span>
                    <br/>
                    <span>Manufactures slashes 30% of manual</span>
                    <br/> 
                    <span>effort with Infor LN implementation</span>
                    </p>
                    <Link href="/transformation" className='text-[#0745D3] mt-8 px-4 hover:font-bold'>Read article</Link>
                </div>
            </div>
        </div> 
    </div>
  )
}

export default CaseStudies