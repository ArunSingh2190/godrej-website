import React from 'react'

const Card = () => {
  return (
    <div className="mt-10 ml-20">
                <h2 className='text-4xl font-bold text-[#374151] ml-20 px-20'>
                    Embrace tomorrow with automation
                </h2>
                <div className='flex space-x-0.5 ml-20 mt-8 px-20 mb-20'>
                    <div>
                        <h4 className='px-12 text-[#374151] font-bold'>
                            Intelligent Technologies
                        </h4>
                        <p className='bg-[#f5f5f5] mt-4 ml-4 p-8 md:leading-[30px] w-80 text-sm text-sky-500'>
                            <span className='hover:text-sky-700'>
                                Artificial Intelligence
                            </span><br/>
                            <span className='hover:text-sky-700'>
                                Machine Learning
                            </span><br/>
                            <span className='hover:text-sky-700'>
                                Robotic Process Automation
                            </span>
                        </p>
                    </div>
                    <div>
                        <h4 className='px-12 text-[#374151] font-bold'>
                            Technology Suite
                        </h4>
                        <p className='bg-[#f5f5f5] mt-4 ml-4 p-8 md:leading-[30px] w-80 text-sm text-sky-500'>
                            <span className='hover:text-sky-700'>
                                Java
                            </span><br/>
                            <span className='hover:text-sky-700'>
                                .Net Share Point
                            </span><br/>
                            <span className='hover:text-sky-700'>
                                Low cost application development
                            </span>
                        </p>
                    </div>
                    <div>
                        <h4 className='px-12 text-[#374151] font-bold'>
                            Data Insights
                        </h4>
                        <p className='bg-[#f5f5f5] mt-4 ml-4 p-4 md:leading-[30px] w-80 text-sm text-sky-500'>
                            <span className='hover:text-sky-700'>
                                Power BI and power apps
                            </span><br/>
                            <span className='hover:text-sky-700'>
                                Birst
                            </span><br/>
                            <span className='hover:text-sky-700'>
                                Big Data
                            </span><br/>
                            <span className='hover:text-sky-700'>
                                Data Lake
                            </span>
                        </p>
                    </div>
                </div>
            </div>
  )
}

export default Card