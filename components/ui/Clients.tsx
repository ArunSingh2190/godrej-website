import React from "react";
import Image from "next/image";
import { companyLogos } from "@/lib/data";

const Clients = () => {
  return (
    <div className="pt-8">
        <h5 className="flex justify-center items-center text-[#101828] font-semibold pb-5">
            500 + organisations trost our exceptional expertise. Join us today!
        </h5>
        <div className="flex justify-center items-center">
            <div className="flex ">
                {companyLogos.map((image, index) => (
                <Image
                key={index}
                src={image.imageUrl}
                className="px-8 w-auto "
                alt={`Company logo ${index + 1}`}
                width={300}
                height={150}
                />
                ))}
            </div>
        </div>
    </div>
  );
};

export default Clients;
