import React from 'react'

const NewsLetter = () => {
  return (
    <div className="bg-[#fef2f2] pb-16"
>       <div className='flex flex-col justify-center text-center'>
            <h2 className='mt-20 text-3xl font-bold font-aria '>
                <span >Subscribe to our newsletter for all</span>
                <br/>
                <span>the latest updates</span>
            </h2>
            <p className='py-1 text-sm font-medium '>
                <span>The latest news, articles, and resources, sent to your inbox</span>
                <br/>
                <span>weekly.</span>
            </p>
        </div>
        <div className='py-12 flex flex-row justify-center space-x-4'>
            <span className='border border-indigo-600 p-1 pr-40'>Email address*</span>
            <span className='border border-indigo-600 p-1 pr-40 bg-black text-gray-100 hover:font-light'>Subscribe</span>
        </div>
    </div>
  )
}

export default NewsLetter