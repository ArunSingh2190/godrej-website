import Image from 'next/image'
import Link from 'next/link'
import React from 'react'

const Footer = () => {
  return (
    <section>
        <div className='bg-[#070021] bg-opacity-90 pb-2'>
            <div className='flex flex-row py-16 px-16 ml-20'>
                <div className='flex flex-col '>
                    <Image src="/logo/godrejInfotech.png" alt="Godrej Infotech Logo" width={200}  height={200} />
                    <p className='text-[#DBDBDB] opacity-100'>
                        <span>Revolutionize your business</span>
                        <br/> 
                        <span>operations, save time and</span>
                        <br/> 
                        <span>boost productivity with our cutting-edge</span>
                        <br/> 
                        <span>offerings.</span>
                    </p>
                </div>
                <div className='flex flex-col text-[#DBDBDB] opacity-100 px-10'>
                    <Link href="/solutions">
                    <h4 className='py-2 font-semibold hover:font-bold'>Solutions</h4>
                    </Link>
                    <Link href="/automation">
                    <h5 className='py-1 font-medium hover:font-bold'>AUTOMATION</h5>
                    </Link>
                    <div className='font-light text-sm'>
                        <h6>Enterprise Suite</h6>
                        <h6>Technology Stack</h6>
                        <h6>Godrej Infotech Products</h6>
                    </div>
                    <Link href="/cloudification">
                    <h5 className='py-1 font-medium hover:font-bold'>CLOUDIFICATION</h5>
                    </Link>
                    <div className='font-light text-sm'>
                        <h6>Upgrade to Cloud</h6>
                        <h6>FullStack</h6>
                        <h6>Upgrade to Cloud</h6>
                    </div>
                    <Link href="/transformation">
                    <h5 className='py-1 font-medium hover:font-bold'>TRANSFORMATION</h5>
                    </Link>
                    <div className='font-light text-sm'>
                        <h6>Intelligent Technologies</h6>
                        <h6>Customer Experience</h6>
                        <h6>Data insights</h6>
                    </div>
                </div>
                <div className='flex flex-col text-[#DBDBDB] opacity-100 px-10'>
                    <Link href="/services">
                    <h4 className='py-2 font-semibold hover:font-bold'>Services</h4>
                    </Link>
                    <div className='font-light text-sm'>
                        <h6>Business Proccess Consulting</h6>
                        <h6>Implementation & Global Roll Out</h6>
                        <h6>Managed Services</h6>
                        <h6>Data & Information Security</h6>
                    </div>
                </div>
                <div className='flex flex-col text-[#DBDBDB] opacity-100 px-10'>
                <Link href="/industryFocus">
                    <h4 className='py-2 font-semibold hover:font-bold'>Industry Focus</h4>
                </Link>
                    <div className='font-light text-sm'>
                        <h6>Manufacturing</h6>
                        <h6>Retail</h6>
                        <h6>Trading & Distrubition</h6>
                        <h6>Project</h6>
                        <h6>Professional Services</h6>
                    </div>
                </div>
                <div className='flex flex-col text-[#DBDBDB] opacity-100 px-10'>
                <Link href="/more">
                <h4 className='py-2 font-semibold hover:font-bold'>More</h4>
                </Link>
                    <div className='font-light text-sm'>
                        <h6>About us</h6>
                        <h6>Infotech Weekly</h6>
                        <h6>Career</h6>
                        <h6>Contact us</h6>
                    </div>
                </div>
            </div>
        </div>
    </section>
  )
}

export default Footer