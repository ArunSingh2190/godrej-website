import React from "react";
import Image from "next/image";
import Link from "next/link";
import Clients from "./ui/Clients";

const Solutions = () => {
  return (
    <section id="solutions" className="bg-[#061826] px-6 md:px-10 pt-20 pb-10">
      <div className="container mx-auto flex flex-col md:flex-row items-center justify-around">
        <div className=" md:pr-8 text-center md:text-left">
          <h1 className="text-6xl text-white font-bold mb-4 pt-8 mt-40 text-center">
            <span>Delivering Business Value </span>
            <br />
            <span className="text-6xl text-white font-bold mb-4 pt-8 text-center">
              with automation
            </span>
          </h1>
          <p className="text-gray-500 mb-8 text-center">
            <span>Revolutionize your business operations, save time and boost productivit</span>
            <br/>
            <span>with our cutting-edge offerings.</span>
          </p>
          <div className="ml-40 space-x-12 py-12 ">
          <Link
            href="/contact"
            className="bg-white px-7 py-2 hover:text-indigo-800 hover:font-bold"
          >
            Lets Connect
          </Link>
          <Link
            href="/corporateVideo"
            className="text-indigo-500 px-7 py-2 hover:text-indigo-600 hover:font-bold inline-block align-baseline underline "
          >
            Co-operate Video
          </Link>
          </div>
        </div>
      </div>
    </section>
  );
};

export default Solutions;
