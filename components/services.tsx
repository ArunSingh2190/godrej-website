import Image from 'next/image'
import React from 'react'
import Card from './ui/Card'

const Services = () => {
  return (
    <section id="services" className="bg-gradient-to-b from-stone-200 via-white to-white ">
        <div className=''>
            <div className='flex justify-center items-center pt-20 mt-20' >
                <h2 className="text-6xl font-bold font-sans md:leading-[70px]">
                <span>Introducing ACT based</span><br/>
                <span>approach for building</span><br/> 
                <span>sustainable solutions</span>
                </h2>
            </div>
            <div className='flex justify-center items-center mt-20 '>
                <Image src="/sustainableImg/sustainableSoln.png" alt="sustainable-solution" width={450} height={450} />
            </div>
            <Card/>
        </div>
    </section>
  )
}

export default Services