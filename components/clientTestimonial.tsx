import Image from 'next/image'
import Link from 'next/link'
import React from 'react'

const ClientTestimonial = () => {
  return (
    <div className='bg-red-200 pb-20 pt-20'>
        <div className='mt-16 pl-20 ml-20'>
            <div className="flex flex-row space-x-6">
                <div className='flex flex-col'>
                    <h5 className='font-bold underline underline-offset-4 text-[#0745D3] pt-20'> CLIENT TESTIMONIALS </h5>
                    <div className='mt-4 grid grid-rows-2 gap-4'>
                        <h2 className='text-3xl font-bold'>
                        <span>Delighted customers</span>
                        <br/>
                        <span>share their success</span>
                        <br/>
                        <span>experience</span>
                        </h2>
                        <p className='text-sans text-[#475467] text-sm'>
                        <span>Discover how we have helped our clients to</span> 
                        <br/>
                        <span>realize tangible outcomes aligned with their</span>
                        <br/>
                        <span>business goals.</span>
                        </p>
                    </div>
                </div>
                <div className='flex flex-col'>
                    <Image src="/testimonial/testimonial1.png" alt="Automation" width={400} height={200}/>
                    <p className='font-medium text-[#475467] py-4 tracking-wide'>
                    “GITL helped us in accelerating digital<br/> 
                    transformation journey with Business<br/> 
                    Central on Azure cloud and Power BI<br/> 
                    solution. As a result, our material<br/> 
                    requisition efficiency has increased by<br/> 
                    66%, time taken in processing purchase<br/> 
                    orders is reduced by 79% and invoice <br/> 
                    processing efficiency has improved by 60%.”
                    </p>
                    <h3 className='font-semibold text-center py-8'>Mrs. Sara Shri</h3>
                </div>
                <div className='flex flex-col'>
                <Image src="/testimonial/testimonial2.png" alt="Automation" width={400} height={200}/>
                    <p className='font-medium text-[#475467] py-4 tracking-wide'>
                    “GITL helped us in accelerating digital<br/> 
                    transformation journey with Business<br/> 
                    Central on Azure cloud and Power BI<br/> 
                    solution. As a result, our material<br/> 
                    requisition efficiency has increased by<br/> 
                    66%, time taken in processing purchase<br/> 
                    orders is reduced by 79% and invoice <br/> 
                    processing efficiency has improved by 60%.”
                    </p>
                    <h3 className='font-semibold text-center py-8'>Mr. Jasprit stanley</h3>
                </div>
            </div>
        </div> 
    </div>
    )
}

export default ClientTestimonial